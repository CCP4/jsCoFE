==================================
What is CCP4 Cloud Client Software
==================================


.. Note::
   This article applies only to CCP4 Setups series 7.0; in later series,
   Client setup is automatic and does not require any manual intervention.


CCP4 Cloud Client is an optional package, which is installed on your local machine
for:

  * seamless integration of CCP4 graphical applications, such as **Coot** with
    remote CCP4 Cloud setups.

  * running CCP4 Cloud locally on your own machine. In local mode, there is no
    connection to remote CCP4 Cloud remote servers, all tasks are run on your
    machine and all your data and projects are kept on your local hard drive.



----------------------------------------------
Do I have to have CCP4 Cloud Client installed?
----------------------------------------------

Remote CCP4 Cloud setups are accessible via common browsers just like any ordinary
web-site, and you can do most of tasks without having Client software installed.
As an example, CCP4 Cloud can be used from tablet devices and smartphones, for
which CCP4 Cloud Clients do not exist.

However, without CCP4 Cloud Client installed, you cannot use certain software,
such as **Coot**, directly with CCP4 Cloud. There will be other functional
limitations as well, for example, you will not be able to process X-ray
diffraction images locally on your machine with either **Xia2**, **DUI/DIALS** or
**iMosflm** software. Besides, CCP4 Cloud Client provides a convenient launcher and
configuration facilities.


----------------------------------------------------
How do I use CCP4 Cloud Client once it is installed?
----------------------------------------------------

Simply use icon launchers, which appear after CCP4 Cloud Client installation, for
starting your working session. Upon double-clicking on icon for either *remote* or *local*
mode, the Client will open a CCP4 Cloud login page in chosen browser, by which time
all needed local software will be already integrated with the system, and you are
fully set to go.
