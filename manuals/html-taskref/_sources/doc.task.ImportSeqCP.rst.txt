================================
Import Sequence(s) by Copy-Paste
================================

This task allows a user to import sequence by copy-pasting them from, e.g.,
other web pages. All instructions are given in the task interface.

.. note::
    several sequences may be copy-pasted in one go, if all of them belong to 
    the same type (protein, DNA or RNA)

.. note::
    CCP4 Cloud will assign sequence names using template name given on top of
    the interface. Default name is *sequence*, therefore, sequence names will 
    be chosen *sequence_01*, *sequence_02*, ... for 1st, 2nd ... sequence
    given. This may be inconvenient for further identification, therefore,
    consider choosing meaningful template names and not importing more than
    one sequence a time.