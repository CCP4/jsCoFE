================================
Split MR model with Slice-n-Dice
================================

Part of Slice-n-Dice automated MR pipeline used to slice predicted models into distinct structural units which are then can be placed in an automatic MR pipeline.

Slice option in CCP4 cloud uses `Birch clustering algorithm <https://scikit-learn.org/stable/modules/clustering.html#birch>`_ based on the coordinates of the Cɑ atoms.


The **number of splits** (clusters) can be specified. The large target protein needs to be modeled in several chunks. An increasing number of splits allows slice and dice to test a range of different splits.


The default **pLDDT threshold** for AlphaFold2 models is **70** and the default **RMS threshold** for RosettaFold models is **1.75**.


**ACKNOWLEDGEMENTS**

This article uses materials kindly provided by Dr. Adam Simpkin and Dr. Ronan Keegan, whose help is greatly appreciated.