.. _shelx:

The programs `SHELXC, SHELXD and SHELXE <https://journals.iucr.org/a/issues/2008/01/00/sc5010/index.html>`__ are designed to provide experimental phasing of macromolecules by the *MAD* (multi-wavelength anomalous dispersion), *SAD* (single-wavelength anomalous dispersion), *SIR* (single isomorphous replacement), *SIRAS* (combined SAD and SIR) and *RIP* (phasing based on radiation-induced changes in the structure) methods.

**SHELXC** is designed to provide a simple and fast way of setting up the files for the programs `SHELXD <https://doi.org/10.1107/S0907444902011678>`_ (heavy atom location) and `SHELXE <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3817699/>`__ (phasing and density modification).

========
SHELXC\D
========

The program SHELXC provides a statistical analysis of the input data, estimates the marker-atom structure factors F\ :sub:`A`\ and the phase shifts α and sets up the files for SHELXD which are used for solving the substructure (i.e. locating the marker atoms).

SHELXC outputs a file ``_fa.hkl`` containing h, k, l, F\ :sub:`A`\ , σ(FA) and the phase shift α for use by SHELXD for substructure solution and by SHELXE for calculating starting phases for the density modification and a file ``_fa.ins`` containing the crystal data and instructions for running SHELXD. ``The α estimates are only required for SHELXE``. SHELXD writes a ``_fa.res`` file in SHELX format for the best substructure solution, which in turn is read by SHELXE. *Content of outputs files you can find at* ``Main Log`` *window.*

In general the critical parameters for locating heavy atoms with SHELXD are:

``1.`` In difficult cases it may be necessary to run more trials 
    (say 50000)

``2.`` **Number of sites**
    requested should be within about 20% of the true value.
    
``3.`` **Resolution cutoff.**
    The resolution at which the data are truncated, e.g. where the internal CC (CC1/2) between the signed anomalous differences of two randomly chosen reflection subsets falls below 30%.

``4.`` **Minimum distance between atoms**
    In the case of a soak, the rejection of sites on special positions should be switched off.
    

more information you can find `here <https://strucbio.biologie.uni-konstanz.de/ccp4wiki/index.php?title=SHELX_C/D/E#SHELXD>`__

======
SHELXE
======

The program `SHELXE <https://journals.iucr.org/a/issues/2008/01/00/sc5010/index.html>`__ was originally designed for the experimental phasing of macromolecules. SHELXE employs the sphere-of-influence algorithm for density modification to improve these phases `[Sheldrick, 2002] <https://www.degruyter.com/view/journals/zkri/217/12/article-p644.xml>`_. It can also prove useful for expanding a small protein fragment to an almost complete polyalanine trace of the structure

SHELXE autotracing algorithm is primarily designed to obtain a toehold in maps with very poor starting phases, e.g. with a mean phase error greater than 60°. The tracing of a polyalanine backbone, combining the phases from the trace with the experimental phases and repeating the density modification iteratively. The tracing proceeds as follows.

    •  Find potential α-helices in the density and try to extend them at both ends. Then find other potential tripeptides and try to extend them at both ends in the same way.

    •  Tidy up and splice the traces as required, applying any necessary symmetry operations.

    •  Use the traced residues to estimate phases, combine these with the initial phase information using σA weights and then restart the density modification. The refinement of one B value per residue provides a further opportunity to suppress wrongly traced residues.
    
The global cycle consists of density modification followed by autotracing include 20 cycles by default. This is usually a good choice, but it might be worth increasing if the solvent content is very high.
    
SHELXE always uses σ\ :sub:`A`\ weights based on the correlation coefficient between the calculated and native E-values as a function of resolution to combine different sources of phase information. A correlation coefficient between the native intensities and those calculated from the trace (CC of a partial structure against native data) is a guide as to whether the structure has been solved or not. In the case of experimental phasing and native data extending to a resolution of 2.5 Å or better, *solutions with a CC of 25% or higher were invariably "solved".*
    
    **Criteria to tell if phasing worked:**
    
    1. Correct hand (naturally occurring proteins consist of l-amino acids (i.e. left-handed amino acids) and right-handed α-helices) shows better contrast, especially at early cycles of density modification.
    
    2. Correct hand has higher map correlation coefficient throughout resolution range.
    
    3. A reasonable poly-ALA trace (average 10 residues per chain) and a CC > 25%

You can find more information about SHELXE  `here <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3817699/>`__

**References**

`Sheldrick, G.M. (2002), "Macromolecular phasing with SHELXE", Z. Kristallogr. 217, 644-650 <https://www.degruyter.com/view/journals/zkri/217/12/article-p644.xml>`_

`Schneider, T.R. & Sheldrick, G.M. (2002). "Substructure Solution with SHELXD", Acta Crystallogr. D58, 1772-1779 <http://scripts.iucr.org/cgi-bin/paper?S0907444902011678>`_

`Sheldrick, G.M. (2008). "A short history of SHELX", Acta Crystallogr. D64, 112-122 <https://journals.iucr.org/a/issues/2008/01/00/sc5010/index.html>`_

`Thorn, A. and Sheldrick, G.M. (2013) Extending molecular-replacement solutions with SHELXE. Acta Cryst D69, 2251-2256. <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3817699/>`_

look also http://shelx.uni-goettingen.de/

