=================================
CCP4 Cloud Education Capabilities
=================================

CCP4 Cloud is not only a powerful tool for managing crystallographic projects 
but also an excellent platform for learning about the CCP4 tasks you are running.

--------------------
CCP4 Cloud Tutorials
--------------------

On the **My Projects** page toolbox, you will find the **Tutorial** |tutorials| icon. 
Clicking on it allows you to import tutorials or demo projects of interest to your 
:ref:`project page<myprojects>`.

**Tutorials** include examples designed to demonstrate structure solution 
techniques in CCP4 Cloud. Tutorials are presented as seed projects with 
pre-loaded data, which can be imported into your account. Each tutorial includes 
step-by-step instructions to guide you through the required project development stages.

You can find a list of all available tutorials `here <../html-tutorials/doc.tutorials.html>`__.

.. |tutorials| image:: images/demoprj.png
    :width: 16
    :height: 16

**Demo projects** represent already developed :ref:`job trees<job-tree>`.

--------------
Task Reference
--------------

.. |reference| image:: images/reference.png
    :width: 16
    :height: 16
    
All tasks runnable in CCP4 Cloud include **reference documentation**. You can 
access task documentation by clicking the **Task Documentation** |reference| 
button in the Job dialog window.

     .. image:: images/src-userguide_images_job_dialog_Ref.png
         :scale: 50 %
         :align: center 

The main purpose of task documentation is to provide knowledge about CCP4 task 
capabilities and to enhance your experience with CCP4 tasks. Task documentation 
not only guides you in utilizing tasks more effectively but also expands your 
knowledge base about methods used to complete tasks.

.. tip::
        You can access additional CCP4 Cloud task documentation by clicking 
        **Contents** in the upper-right corner of the window. The contents page 
        includes a list of all available task documentation.

+++++++++++++++++++++++++
Questions and Suggestions
+++++++++++++++++++++++++

At the end of both the User Guide and Task Documentation contents pages, 
you will find the `CCP4 Cloud Reception`_. By clicking it, you will be redirected 
to a mail form where you can submit questions or suggestions about working 
in CCP4 Cloud, as well as feedback on task documentation content.

.. _CCP4 Cloud Reception: mariya.fando@gmail.com?Subject=CCP4%20Cloud%20question
