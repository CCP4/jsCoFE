===============
Troubleshooting
===============

.. contents::
   :depth: 2

-----------------------------------------------------
Some tasks cannot be created or added to the job tree
-----------------------------------------------------

If a task cannot be created and added to the :ref:`Job Tree <job-tree>`, its icon in the :ref:`task-list` appears within a red frame, and the corresponding list entry is grayed out. Clicking on the red-framed icon will not start the *Job dialog* for the task. Instead, an explanation will be provided, which may be one of the following:

* **Not all required input data is available:**  
   Tasks rely on data produced by their ancestors in the :ref:`Job Tree <job-tree>`. If the necessary input data is missing, you may need to import it or run another task to generate it. For example, ``Phaser`` and ``Molrep`` require molecular replacement models or ensembles prepared from atomic coordinates or sequences. If the required data exists in other branches of the :ref:`Job Tree <job-tree>`, select those jobs by holding down the "*Ctrl*" (Windows, Linux) or "*Apple*" (Mac OSX) key and clicking on the relevant jobs. Select the desired parent job last, then right-click to add the new task.

* **One or more required data objects are outdated:**  
   This situation may occur for the same reason as the :ref:`impossibility to clone due to incompatibility <cant-clone-1>`. Please refer to that section for more details.

.. _task-availability:

* **The task requires CCP4 Cloud Client:**  
   This occurs when connecting to CCP4 Cloud via a direct web link (e.g., `https://cloud.ccp4.ac.uk <https://cloud.ccp4.ac.uk>`_). Some tasks can only run on your local computer, which is possible only when connecting to CCP4 Cloud through the CCP4 Cloud icon in your local CCP4 Setup. This icon launches the CCP4 Cloud Client, enabling interactive tasks such as ``Coot``.

* **The task is not available for the current operating system (typically MS Windows):**  
   Switch to a compatible operating system, such as Linux or Mac OSX.

* **A required software component is missing:**  
   Contact the CCP4 Cloud maintainer to request the installation of the necessary software.

* **The task requires** :ref:`software-authorisation` **from the software provider:**  
   Some third-party software requires user registration for licensing purposes. Navigate to "My Account" in the top-left menu of the CCP4 Cloud page, click the fingerprint icon, and follow the instructions to complete the registration process (see :ref:`software-authorisation`).

-------------------------
Job seems to never finish
-------------------------

This issue may occur for the following reasons:

* **The job is time-consuming and appears stalled:**  
   Automatic structure-solving jobs can take significant time, sometimes even weeks, depending on the input data and selected parameters. Inspect the "**Main Log**" tab in the Job dialog. If the log is not advancing for a long period, the job may be stalled. If the log shows signs of infinite looping, the job may have run away (see the next point). Otherwise, the job is running. You may :ref:`stop it <stop-job>` if desired.

* **The job has entered an infinite loop:**  
   This could be due to program bugs or algorithmic issues. If you suspect this, :ref:`stop the job <stop-job>`.

* **Communication errors:**  
   After job completion, results are sent back to your project. Connection issues may cause this process to fail despite multiple attempts. Job results are retained for up to one month (or as configured) and may be retrieved by pressing the :ref:`Refresh <refresh-project>` button on the **Project Page**. If the connection issues are resolved, this should properly retrieve the job results.

* **Technical faults:**  
   If a runaway job cannot be stopped or a completed job cannot be retrieved even after multiple refresh attempts, report the issue to the CCP4 Cloud maintainer. The job may be lost due to a system malfunction. Frequent technical faults may indicate bugs, poor connections, or other issues that require attention.

.. _stop-job:

**Stopping a job**

To stop a job, press the "**Stop**" button in the Job dialog. The job should stop within a few minutes, often much faster. Avoid deleting running jobs, as this only removes the job from the project but *does not actually stop it*.

.. _cant-clone:

----------------------------------------------
CCP4 Cloud says that the task cannot be cloned
----------------------------------------------

This issue may occur due to the following reasons:

.. _cant-clone-1:

* **Backward compatibility issues with task modules:**  
   The task may have been updated, making it incompatible with previous versions. Create the task anew from the parent mode instead of cloning. If data incompatibility issues arise with the parent or higher ancestors, you may need to repeat parent or even grandparent tasks in the :ref:`Job Tree <job-tree>`.

* **Cloning attempted in a different setup where the task is unavailable:**  
   This may occur due to the same reasons described in :ref:`impossibility to create a new task <task-availability>`. Please refer to that section.