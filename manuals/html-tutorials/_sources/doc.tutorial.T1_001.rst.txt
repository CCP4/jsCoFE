===============================
Data processing and SAD phasing
===============================


This tutorial is based on a set of diffraction images collected from a crystal of selenomethionine-containing CD44 protein.

``1.`` The protein sequence has been already imported and can be found in the output of the **Cloud Import** task.

``2.`` The next step is to process diffraction images to obtain merged reflection dataset. This can be automatically with the `Automatic Image Processing with Xia2 <https://cloud.ccp4.ac.uk/manuals/html-taskref/doc.task.Xia2.html>`_ task: **add next job => All tasks => Data Processing => Automatic Image Processing with Xia-2**.

**Image directory**

      *diffraction images are found in Cloud storage at the following path:* **Tutorials/Tutorial data/1_from_images/cd44/images**

Specify **Se** as **Heavy atom type** in the task input.

.. note:: Resolution limits and Spacegroup are normally determined by Xia-2 automatically, but it is possible to override the automatic choice if necessary.

.. note:: Xia2 outputs two or more unmerged datasets, and one merged dataset. The latter contains anomalous data only if heavy atom type is specified in task input parameters. Otherwise, only mean intensities will be computed.


.. toggle-header::
        :header:  **Xia-2 report**

                  **Look for the plot of CC(1/2) vs resolution** (**Dataset SAD** => **Analysis by resolution** section). It shows the correlation coefficient between randomly selected half sets of the intensities in the dataset. Low (less than 0.5) values of CC(1/2) indicate internally inconsistent data (weak signal data) and *it is a good idea not to include such data from processing*. By default, Xia2 cuts off data at a resolution where CC(1/2) drops to 0.5 (some people are less conservative and select a cutoff closer to 0.35).

                  **Completeness** describes the percentage of the theoretically observable reflections (within given resolution limits) that have been actually measured in the experiment. Completeness should be used for estimating whether the data is principally sufficient for structure determination. A value greater than 90% is generally desired, while a value less than 75% is considered to be poor. Values in between of these limits will provide less than optimal results.

                  **Look for the plot of Rmerge vs Batch for all images** (it is found in the **Dataset SAD** => **Analysis by batch** section). **Rmerge** shows how well multiple observations of equivalent reflections agree with each other, with a low value indicating better agreement. Note that this value is stable throughout much of the dataset, but starts to drift upwards towards the end of data collection, indicating a possible radiation damage to the crystal. It is often beneficial to remove these compromised data from furgher processing, for as long as we can leave ourselves with the acceptable degree of completeness. **For this tutorial's dataset, removing images starting from 121 on represents a reasonable compromise**.


``3.`` While it is possible to use the merged reflection dataset, produced by Xia-2, straight away, manual data reduction and merging gives more control and may result in better data quality. Let's do that with `Aimless <https://cloud.ccp4.ac.uk/manuals/html-taskref/doc.task.Aimless.html>`_: **add next job => All tasks => Data Processing => Merging and Scaling with Aimless**

In Aimless input parameters, exclude the most radiation damaged images from processing. Choose **xia2_unmerged_scaled** data in **Unmerged reflections** section. **Limit number batches to 120** (type 1-120 in the **Batches**). Set a **high resolution cut-off** at **1.8 Å**.

``4.`` After all needful data (sequence and merged reflection dataset with anomalous signal in SAD case), first approximation to structure factors phases must be obtained. In CCP4 Cloud, phasing procedure starts with making a hypothesis on crystal composition, that is, guessing the correct number of protein chains in the Asymmetric Unit. Good guess is important for making correct estimation of solvent fraction; grossly incorrect estimate may have a significant negative impact on the success of Experimental Phasing. In order to specify the expected crystal composition, run the `Asymmetric Unit definition <https://cloud.ccp4.ac.uk/manuals/html-taskref/doc.task.ASUDef.html>`_ task, which will also calculate the corresponding solvent content. **add next job => All tasks => Asymmetric Unit and Structure Revision => Asymmetric Unit Contents**

When preparing the task for run, specify **Se** as **Main anomalous scatterer**.

After the task finishes, inspect **verdict** section in the report page. The verdict will help you to assess the likelihood of having correct ASU composition.

.. note:: ASU composition remains a hypothesis until the structure is solved. It is not unusual to have it reconsidered in case of difficulties with phasing or refinement.

On output, the task creates the initial **structure revision**, containing only **ASU definition**  and **merged HKL dataset**.

``5.`` Once the Asymmetric Unit is defined and the initial structure revision is created, first approximation to phases may be computed. Finding initial phases in Experimental Phasing is a two-step procedure. In first step, anomalous signal in the reflection dataset is used to find the location of anomalous scatterers (the heavy-atom substructure). In second step, the reflection dataset and substructure found are used to estimate protein phases.

The substructure is found with ShelxCD software, which may be used as part of Crank-2 automatic pipeline (see task **Substructure Search with SHELX via Crank-2** in the Experimental Phasing section of the Task List), or with the dedicated ShelxCD task (**Substructure Search with SHELX-C/D**). Let’s choose the first one: **add next job => All tasks => Experimental phasing => Substructure Search with SHELX via Crank-2**.

.. toggle-header::
          :header:  **Output**

            *the task provides heavy atom substructure for calculating initial protein phases*

            **FA estimation and/or other substructure detection preparations**

            The graph **Dano/Sigdano** shows the anomalous signal strength as a function of resolution. The larger the **Dano/Sigdano** ratio, the stronger the signal; values under 0.7 indicate a very weak signal not suitable for phasing (greyed area in the plot). In our case, the anomalous signal appears to be relatively weak but still sufficient for SAD phasing.

.. toggle-header::
          :header: **Substructure determination output**

            Please find the report summary for the substructure determination under **graph data**.

.. toggle-header::
          :header: **Result**:

            **Judging from the scores obtained, there is a reasonable chance that the substructure has been correctly determined**

            A good indication that the substructure has been correctly determined is if CFOM reaches the *automatically set threshold* before the maximum number of cycles has been made.

            .. note:: this indication may be invalid if automatic choice of CFOM threshold is overridden in the tasks input parameters.

            A clear separation between low-value and high-value CFOM distributions in the **distribution of CFOM from trials** graph is another an encouraging indication. In this tutorial, the graph shows good separation of unsuccessful (lower CFOM) trials and successful (higher CFOM) solutions.

            The success of substructure determination can be also guessed from the trials clustering pattern in **CC against CCweak** plot. If cluster, located in the top-right corner of the plot, lies above CC=0.25 and is well-separated from the cluster of unsuccessful trials in the bottom-left part of the plot, the likelihood of correct substructure determination is significant.

            **Estimated atom occupancies** shows estimated atom occupancies in the substructure found, as given by the best trial. An atom is considered as more or less reliably located if **Estimated occupancy is higher than 0.25**.


``6.`` Macromolecular phases are calculated using substructure phases and reflection data with **Phaser-EP** software. The Phaser-EP task calculates protein phases for the original and inverted hands, corresponding to mutually-inversed heavy-atom substructures. Run the task: **add next job => All tasks => Experimental phasing => Experimental Phasing with Phaser-EP**

The task will improve the substructure, calculate inverted substructure and estimate their macromolecular phases.

.. Note::
        do not mistake substructure phases, calculated in ShelxCD task, with macromolecular phases

Examine the **verdict** section in Phaser output, which contains the analysis of task performance.

The average **FOM** (Figure Of Merit) is the phase quality indicator: FOM values **above 0.3** correspond to **good phases** (which may lead to an interpretable electron density map) and values **below 0.15** indicate **poor phases** (which make model building difficult or impossible).

``7.`` Phaser-EP job produces two structure revisions, one for the original and one for the inverse hands. Unless the heavy-atom substructure has inversion centre, only one hand's phases are suitable for model building. At high quality of phases, the correct hand can be identified by visual inspection of the corresponding electron density maps (try this). However, in most cases, the phases need to be improved (modified) in order to make a confident choice. Even more, automatic building in both hands may need to be attempted in order to make the ultimate decision. Therefore, perform density modification in each hand  using the `Density Modification with Parrot <https://cloud.ccp4.ac.uk/manuals/html-taskref/doc.task.Parrot.html>`_ task.

.. note:: the density modification jobs should both descend from Phaser-EP job, because they need structure revisions created by it. This means that project tree will branch at this point. To make your project easily interpretable, annotate the branches by starting them with appropriate remarks: first, place remark “Original hand”, then clone it and rename as “Inverted hand”.

``8.``	Append Parrot task to the “Original hand” remark (**add next job => All tasks => Density Modification => Density Modification with Parrot**) and make sure that the “original hand” revision from Phaser-EP (**phaser-ep-original_hand**) is selected in **Structure revision**. State **heavy-athom(Se) substructure** as **model for NCS detection**. Parrot uses substructure to define initial values of NCS operations. Start the task.

``9.``	Append Parrot task to the “Inverted hand” remark (**add next job => All task => Density Modification => Density Modification with Parrot**) and make sure that the “inverted hand” revision from Phaser-EP **phaser-ep-inverted_hand** is selected in **Structure revision**. State **heavy-athom(Se) substructure** as **model for NCS detection**. Start the task.

.. note:: jobs in different branches can be run simultaneously, therefore, there is no need to wait for Parrot job in the "Original hand" branch to finish before starting Parrot job in the "Inverted hand" branch!

Inspect electron density generated by the two Parrot runs in **UglyMol** viewer (the corresponding launch button is found in *task output*).

``10.`` Are resulting maps from Parrot jobs good enough for deciding on the hand? *Probably not*. Therefore, continue each project branch with the `Automatic Model Building with CCP4Build <https://cloud.ccp4.ac.uk/manuals/html-taskref/doc.task.CCP4Build.html>`_ task: **add next job => All tasks => Refinement and Model Building => Automatic Model Building with CCP4Build**

Now the correct hand can be identified with certainty.

Could using more cycles of model building be useful?

*You may clone and run another CCP4Build job(s) if you think it could*

``11.``  Further refinement can be conveniently done by the **Automated refinement and ligand fitting workflow**

.. image:: images/Demo.CCP4MG_workflow.png
        :align: center
        :scale: 40 %

``12.``  Run the **Prepare data for deposition** task and check the PDB Validation Report to see whether the structure quality is good enough for deposition.

*As a rule, automatically built and refined structures do not achieve deposition quality standards and need to be refined more thoroughly with the interactive model building (Coot)*. Start **Model Building with Coot** as a child node of the Refmac job and correct several side chains.

.. note:: Coot task is available only if CCP4 Cloud is started through the local installation of CCP4 using launcher with |ccp4cloud_remote| icon.

Refine structure after editing in Coot. Have you got R-factors decreased?

.. |ccp4cloud_remote| image:: images/ccp4cloud_remote.png
   :width: 20
   :height: 20
