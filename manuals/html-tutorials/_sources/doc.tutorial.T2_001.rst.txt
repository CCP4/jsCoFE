===============================================
Creating MR Search Ensembles with CCP4MG/MrBUMP
===============================================

This tutorial explains how to create the ensembles for Molecular Replacement (MR) with `CCP4mg <https://www.ccp4.ac.uk/MG/>`_ molecular viewer. We will use data from CCP4's Gamma protein example.

  .. important::
             Launch CCP4 Cloud through the local installation of CCP4 on your computer |ccp4cloud_remote|.

            .. |ccp4cloud_remote| image:: images/ccp4cloud_remote.png
                :width: 20
                :height: 20

``1.``	 The sequence and reflection data were already imported in the “Creating MR Search Ensembles with CCP4MG/MrBUMP” project (*import from the Cloud storage* task).

``2.``	The next step is to define the **Asymmetric Unit content**: click **add task => go to All tasks => Asymmetric Unit and Structure revision => Asymmetric Unit Contents.** Run the task.

.. image:: images/Demo.CCP4MG_ASU.png
   :align: center
   :scale: 40 %

Every task in CCP4 Cloud produces a report. Some reports come with the verdict. 

.. image:: images/Demo.CCP4MG_ASU2.jpg
   :align: center
   :scale: 30 %

.. tip::
        Please read verdict carefully, it helps assessing the obtained results. 

``3.``	Once **Asymmetric Unit Contents** is defined with acceptable scores (solvent content, Matthews probability and expected stoichiometry), start the **Prepare MR Ensemble with CCP4mg** task: **add next job => All tasks => Molecular Replacement => Prepare MR Ensemble with CCP4mg**

.. image:: images/Demo.CCP4MG_MG1.png
   :align: center
   :scale: 40 %

``4.``	`Prepare MR Ensemble with CCP4mg <https://cloud.ccp4.ac.uk/manuals/html-taskref/doc.task.EnsemblePrepMG.html>`_ task will identify structure homologs from the `PDB <https://www.rcsb.org/>`_ and the `EBI-AFDB <https://alphafold.ebi.ac.uk>`_ and display them in CCP4mg (``takes a couple of minutes``).

For :abbr:`AFDB (AlphaFold DataBase)` models, you can adjust the pLDDT cut-off

.. toggle-header::
       :header:  **pLDDT cut-off**
                
                AlphaFold produces a per-residue estimate of its confidence score, called **pLDDT**, ranging from 0 to 100. This option sets the minimum level of **pLDDT** of selected residues.
                
                * **pLDDT > 90**: residues are expected to be positioned with a high accuracy. These models should be suitable for any application that benefits from high accuracy (e.g. characterising binding sites).
                
                * **70 < pLDDT < 90** residues are expected to be positioned reasonably well (generally, a good backbone prediction).
                
                * **50 < pLDDT < 70** corresponds to residues modelled with low confidence, which should be treated with caution.
                
                * **pLDDT < 50** indicates residues that should not be interpreted. Such scores are a reasonably strong predictor of disorder, i.e. they suggest that the region is either unstructured in physiological conditions, or gets structured only as part of a complex.


Higher values mean only the most confidently predicted residues are retained in the search models.


.. Note::
        It is possible to search only the PDB or EBI-AFDB by unticking the relevant box in the input menu.

.. tip::
      In order to read more abound the task click on task documentation button |reference|


.. |reference| image:: images/reference.png
    :width: 20
    :height: 20


For this tutorial, make sure that:

    •	**PDB sequence redundancy level** set to **100%**

    •	**Include structures from AFDB** set **on**

    •	**EBI-AFDB pLDDT cut-off** set to **50%**

    •	**Maximum number of models to create** is **10**


.. image:: images/Demo.CCP4MG_MG2.png
    :align: center
    :scale: 35 %


``5.``	Shortly after the task is run, the **CCP4mg window** will opened on your computer. After a couple of minutes, results of the MrBUMP sequence-based search should appear in the graphical interface.

  .. Note::
       the models will be truncated to better match the target structure by referencing the sequence alignment between the model and the target. The `Sculptor <https://doi.org/10.1107/S0907444910051218>`_ application is used to perform the truncation. The resulting search models are structurally aligned and superposed.

 .. image:: images/Demo.CCP4MG_MG3.png
    :align: center
    :scale: 30 %

``6.``	In order to save all that is currently visible in CCP4mg and make it an “ensemble model” go to **File => Save all visible to the CCP4 Cloud**

  .. tip::
    To save a single model or chosen models, make all other models invisible. To do this in CCP4mg, right-click (or Ctrl-click / Cmd-click on a laptop) on the coloured icon next to the model’s name in the “Display Table” (right-hand panel of CCP4MG window). The icon turns grey and the model becomes invisible in the graphics window. Right-clicking again makes the model visible again.


``7.``	Quit CCP4mg

    The created ensemble should appear in the task output window.

.. image:: images/Demo.CCP4MG_MG4.png
    :align: center
    :scale: 40 %

``8.``	Created ensembles can be further used in both `Phaser <https://cloud.ccp4.ac.uk/manuals/html-taskref/doc.task.Phaser.html>`_ and `Molrep <https://cloud.ccp4.ac.uk/manuals/html-taskref/doc.task.Molrep.html>`_ tasks.

    We will use Phaser for Molecular replacement in this tutorial: **add next job => All tasks => Molecular Replacement => Molecular Replacement with Phaser**

    For predicted (AFDB) models, we recommend setting **Similarity to target by rms differences (Å) to 1.2**

.. image:: images/Demo.CCP4MG_Phaser.jpg
        :align: center
        :scale: 30 %

``9.``  After Phaser, the positioned model should be rebuilt in order to match the target sequence more closely. This can be conveniently done using one of CCP4's automatic model builders (`Buccaneer <https://cloud.ccp4.ac.uk/manuals/html-taskref/doc.task.Buccaneer.html>`_, `Arp/wArp <https://cloud.ccp4.ac.uk/manuals/html-taskref/doc.task.ArpWarp.html>`_ , `CCP4Build <https://cloud.ccp4.ac.uk/manuals/html-taskref/doc.task.CCP4Build.html>`_). Let's run the `ModelCraft <../html-taskref/doc.task.ModelCraft.html>`_ task. The built model can be visually inspected with **UglyMol** (the corresponding launch button can be found in task report page).

``10.``	 Further refinement can be done with the **Automated refinement and ligand fitting workflow**

.. image:: images/Demo.CCP4MG_workflow.png
        :align: center
        :scale: 40 %
