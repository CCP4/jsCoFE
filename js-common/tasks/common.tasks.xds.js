
/*
 *  =================================================================
 *
 *    01.06.24   <--  Date of Last Modification.
 *                   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  -----------------------------------------------------------------
 *
 *  **** Module  :  js-common/tasks/common.tasks.xds.js
 *       ~~~~~~~~~
 *  **** Project :  jsCoFE - javascript-based Cloud Front End
 *       ~~~~~~~~~
 *  **** Content :  XDS Run Preparation Task Class
 *       ~~~~~~~~~
 *
 *  (C) E. Krissinel, A. Lebedev 2024
 *
 *  =================================================================
 *
 */

'use strict';

var __template = null;
var __cmd      = null;

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined')  {
  __template = require ( './common.tasks.xia2' );
  __cmd      = require ( '../common.commands' );
}

// ===========================================================================

function TaskXDS()  {

  if (__template)  __template.TaskXia2.call ( this );
             else  TaskXia2.call ( this );

  this._type       = 'TaskXDS';
  this.name        = 'xds';
  this.setOName ( 'xds' );  // default output file name template
  this.title       = 'Image Processing with XDS';
  this.autoRunId   = '';
  this.nc_type     = 'client-storage';  // job may be run only on either client NC or
                                      // ordinary NC if cloud storage is there

  this.maxNDirs    = 1; // maximum number of input directories
  this.hdf5_range  = '';       // for processing HDF5 files in blocks
  this.datatype    = 'images'; //  images/hdf5
  this.file_system = 'local';  //  local/cloud

  this.parameters = { // input parameters
    sec1  : { type     : 'section',
              title    : 'Parameters',
              open     : true,  // true for the section to be initially open
              position : [0,0,1,8],
              contains : {
                MODE_SEL : {
                        type     : 'combobox',
                        keyword  : 'MODE',
                        label    : 'Start processing tasks',
                        tooltip  : 'Choose the level of manual intervention in ' +
                                   'starting XDS processing tasks. Choose '      +
                                   '"Semi-automatic" or "Manual" modes if you '  +
                                   'wish to revise task parameters before '      +
                                   'runnung.',
                        range    : ['A|Automatically',
                                    // 'S|Semi-automatically',
                                    'M|Manually'
                                   ],
                        value    : 'A',
                        iwidth   : 200,
                        position : [0,0,1,1]
                      },
                SET_UNTRUSTED_CBX : {
                        type     : 'checkbox',
                        label    : 'Set untrusted detector areas',
                        tooltip  : 'Check to include step for setting untrusted ' +
                                   'detector areas',
                        value    : false,
                        iwidth   : 240,
                        position : [1,0,1,3]
                      }
              }
            }
  };

  this.saveDefaultValues ( this.parameters );

}

if (__template)
  __cmd.registerClass ( 'TaskXDS',TaskXDS,__template.TaskXia2.prototype );
else    registerClass ( 'TaskXDS',TaskXDS,TaskXia2.prototype );

// ===========================================================================

TaskXDS.prototype.icon                = function() { return 'task_xds'; }
TaskXDS.prototype.clipboard_name      = function() { return '"XDS"';    }
TaskXDS.prototype.requiredEnvironment = function() { return ['CCP4','XDS_home']; }

// TaskXDS.prototype.lowestClientVersion = function() { return '1.6.001 [01.01.2019]'; }

TaskXDS.prototype.currentVersion      = function() {
let version = 0;
  if (__template)
        return  version + __template.TaskXia2.prototype.currentVersion.call ( this );
  else  return  version + TaskXia2.prototype.currentVersion.call ( this );
}

// default post-job cleanup to save disk space
TaskXDS.prototype.cleanJobDir = function ( jobDir )  {}

TaskXDS.prototype.checkKeywords = function ( keywords )  {
// keywords supposed to be in low register
  return this.__check_keywords ( keywords,['xds','image','processing'] );
}

// export such that it could be used in both node and a browser
if (!__template)  {
  // for client side

  TaskXDS.prototype.desc_title = function()  {
  // this appears under task title in the task list
    return 'performs X-ray diffraction data processing';
  };

  TaskXDS.prototype.layDirLine = function ( inputPanel,dirNo,row )  {
    let dir_input = TaskXia2.prototype.layDirLine.call ( this,inputPanel,dirNo,row );
    if (inputPanel.datatype=='images')  {
      if ('sectors_inp' in dir_input)
        for (let i=0;i<dir_input.sectors_inp.length;i++)  {
          dir_input.sectors_inp[i].setReadOnly ( true );
          dir_input.sectors_inp[i].setTooltip  ( 'Image range for your information' );
        }
    } else  {
      dir_input.label     .setVisible ( false );
      if (('hdf5_range' in dir_input) && dir_input.hdf5_range)
        dir_input.hdf5_range.setVisible ( false );
    }
    return dir_input;
  }

  TaskXDS.prototype.customDataClone = function ( cloneMode,task )  {
    TaskXia2.prototype.customDataClone.call ( this,cloneMode,task );
    this.autoRunId  = '';
    this.autoRunId0 = '';
    return;
  }

  // hotButtons return list of buttons added in JobDialog's toolBar.
  TaskXDS.prototype.hotButtons = function() {
    return [XDS3HotButton()];
  }

  TaskXDS.prototype.collectInput = function ( inputPanel )  {
    let input_msg = TaskXia2.prototype.collectInput.call ( this,inputPanel );
    if (this.parameters.sec1.contains.MODE_SEL.value!='M')
          this.autoRunId = 'auto-XDS';
    else  this.autoRunId = '';
    return input_msg;
  }

  // reserved function name
  //TaskXDS.prototype.runButtonName = function()  { return 'Import'; }

} else  {
  // for server side

  const conf  = require('../../js-server/server.configuration');

  TaskXDS.prototype.getCommandLine = function ( jobManager,jobDir )  {
    return [conf.pythonName(), '-m', 'pycofe.tasks.xds', jobManager, jobDir, this.id];
  }

  module.exports.TaskXDS = TaskXDS;

}
