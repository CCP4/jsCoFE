#!/bin/bash

server_dir=/Users/eugene/Projects/jsCoFE
ccp4_dir=/Applications/ccp4-8.0
pdb_dir=/Users/eugene/pdb/pdb
gesamt_dir=/Users/eugene/pdb/gesamt_archive_s
xds_dir=/Applications/XDS
xdsgui_dir=/usr/local/bin

source $ccp4_dir/bin/ccp4.setup-sh
#source $morda_dir/morda_env_osx_sh

# export PDB_DIR=$pdb_dir
export GESAMT_ARCHIVE=$gesamt_dir
#export JSPISA_CFG=$CCP4/share/pisa/jspisa.cfg
export XDS_home=$xds_dir
export XDSGUI_home=$xdsgui_dir
export DOCREPO=/Users/eugene/Projects/jsCoFE/jscofe-doc

# custom plugin for Xia-2
export Xia2_durin=/path/to/durin-plugin.so

# custom script for AlphaFold
# export ALPHAFOLD_CFG=1

# internal variable for mounting directopry with CCP4(i2) demo data
export CDEMODATA=`ccp4-python -c "import sysconfig; print(sysconfig.get_path('purelib'))"`/ccp4i2/demo_data
export CTUTORIALS=/Users/eugene/Projects/jsCoFE/cloud_tutorials

# comment out if buster is not installed
source /Applications/GPhL/Buster/setup.sh

cd $server_dir

echo $PATH
echo $PYTHONPATH
which ccp4-python
echo $XDS_home
echo $XDSGUI_home
echo $DOCREPO
echo $Xia2_durin
which gemmi
gemmi --version
echo ""

#killall node

node --trace-warnings ./js-server/desktop.js ./config/conf.desktop-tutorials.json
#while [ $? -eq 212 ]
#do
#    node ./js-server/desktop.js ./config/conf.desktop.json
#done
#node ./desktop.js ./config/conf.desktop.json -localuser 'Eugene Krissinel'
